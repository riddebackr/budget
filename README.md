# Budget
Budget helps you control your Mastodon's server bills and donations and share current financial status with your users by posting them via the publisher bot.

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon's bot account

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed Python libraries.

2. Run `python db-setup.py` to setup and create new Postgresql database and needed tables in it.  

3. Run `python setup.py` to get your Mastodon's bot account tokens and to be able to post your server's financial status.

4. Run `python budget.py` to manage or list your bills and donations.  

5. Run `python addfee.py` to add your regular fees.  

6. Use your favourite scheduling method to set `python publisher.py` to run regularly. It will post your finances to your fediverse server.

